variable "environment" {
  type = string
}
variable "product" {
  type = string
}
variable "use_case" {
  type = string
}
variable "cluster_config" {
  type = any
}
variable "ssh_key_bucket_name" {
  type    = string
  default = ""
}
variable "save_private_key" {
  type    = bool
  default = false
}
variable "need_karpenter_tags" {
  type    = bool
  default = false
}
variable "node_security_group_tags" {
  type    = map(any)
  default = {}
}